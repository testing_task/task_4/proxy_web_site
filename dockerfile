FROM nginx:alpine

COPY ./nginx/default.conf /etc/nginx/conf.d

COPY ./nginx/bs-biwas /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
